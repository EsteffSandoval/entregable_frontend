import { LitElement, html } from 'lit-element'; 

class PersonaSidebar extends LitElement {
	static get properties() {
		return {			
	    };
    }

	constructor() {
		super();			
	}

	render() {
        return html`	
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
            <aside>
                <section>				
                    <div class="mt-5">
                        <center><button @click="${this.newPerson}" class="btn btn-outline-primary" style="font: 200% sans-serif;"><strong>Nueva Persona</strong></button></center>
                    <div>				
                </section>
            </aside>		
            `;
    }

    newPerson(e) {
        console.log("newPerson en persona-sidebar");
        console.log("Se va a crear una nueva persona");
      
        this.dispatchEvent(new CustomEvent("new-person", {})); 
    }

}

customElements.define('persona-sidebar', PersonaSidebar)