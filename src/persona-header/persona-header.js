import { LitElement, html } from 'lit-element';

class PersonaHeader extends LitElement {

    static get properties() {
        return {
        };
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
            <div style="background: rgb(240, 240, 240); font-family: Verdana; padding: 10px 10px;">
                <h1>App Persona</h1>
            </div>
        `;
    }
}

customElements.define('persona-header', PersonaHeader)