import { LitElement, html } from 'lit-element';

class PersonaFichaListado extends LitElement {

    static get properties() {
        return {
            name: {type: String},
            yearsInCompany: {type: Number},
            profile: {type: String},
            photo: {type: Object}
        };
	}

    constructor() {
		super();
		this.name="";
		this.yearsInCompany=0;
		this.profile="";
		this.photo={};
    }

    render() {
		return html`
				<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
				<div class="card text-center" style="width: 18rem; display: inline-block; margin:10px 10px; font: 100% sans-serif;">
				<div class="card-header">
				<img src="${this.photo.src}" alt="${this.photo.alt}" height="150px" class="card-img-top">
				</div>
				<div class="card-body">
					<h5 class="card-title"><strong>${this.name}</strong></h5>
					<p class="card-text">${this.profile}</p>
					<ul class="list-group list-group-flush">
						<li class="list-group-item">${this.yearsInCompany} años en la empresa</li>
					</ul>
				</div>
                <div class="card-footer text-muted">
                    <center><button @click="${this.deletePerson}" class="btn btn-primary"><strong>Eliminar</strong></button></center>
                </div>				
            </div>
        `;
    }

    deletePerson(e) {	
		console.log("deletePerson en persona-ficha-listado");
		console.log("Se va a borrar la persona de nombre " + this.name); 

		this.dispatchEvent(
			new CustomEvent("delete-person", {
					detail: {
						name: this.name
					}
				}
			)
		);
	}
}

customElements.define('persona-ficha-listado', PersonaFichaListado)