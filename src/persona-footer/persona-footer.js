import { LitElement, html } from 'lit-element';

class PersonaFooter extends LitElement {

    static get properties() {
        return {
        };
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
            <div style="background: rgb(240, 240, 240); font-family: Verdana; padding: 10px 10px; margin: 10px 10px;">
                <h5 style="text-align: right;">@Persona App 2020</h5>
            </div>
        `;
    }
}

customElements.define('persona-footer', PersonaFooter)